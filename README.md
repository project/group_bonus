# Group Bonus

This module adds various small tweaks on top of the Groups module.

## Install / Setup

1. Download this module
2. Enable the module
3. Ready to go!

## Features

1. Redirect users back to the content (node) after saving group content forms.
2. If using the [linkit](https://www.drupal.org/project/linkit) module will also show the name of the group.
3. Adds tab link on content (node) to find the group.
