<?php

/**
 * @file
 * Adds some bonus features to the group module in D8.
 */

use Drupal\group\Entity\GroupContent;
use Drupal\Core\Entity\FieldableEntityInterface;
use Drupal\Core\Entity\EntityInterface;
use Drupal\Core\Cache\Cache;
use Drupal\group\Entity\GroupContentInterface;
use Drupal\node\NodeInterface;
use Drupal\Core\Form\FormStateInterface;

/**
 * Implements hook_form_BASE_FORM_ID_alter().
 */
function group_bonus_form_group_content_form_alter(&$form, FormStateInterface $form_state, $form_id) {
  // Adds submit handler that will alter the redirect of the form.
  $form['actions']['submit']['#submit'][] = 'group_bonus_group_content_form_submit';
}

/**
 * Additional submit handler for group content forms.
 *
 * Will redirect the user to the content (node) page after saving it instead of
 * redirecting to the group content entity view page.
 */
function group_bonus_group_content_form_submit(array $form, FormStateInterface $form_state) {
  $entity = $form_state->getFormObject()
    ->getEntity()
    ->get('entity_id')
    ->referencedEntities();
  if (!empty($entity)) {
    $entity = reset($entity);
    // Although this is easy to implement for any content type, we do it only
    // for nodes, becasue we are sure that we have a full page view for them.
    if ($entity instanceof NodeInterface) {
      $route_params = ['node' => $entity->id()];
      $form_state->setRedirect('entity.node.canonical', $route_params);
    }
  }
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function group_bonus_group_content_update(GroupContentInterface $entity) {
  group_bonus_invalidate_cache_for_group_content($entity);
}

/**
 * Implements hook_ENTITY_TYPE_update().
 */
function group_bonus_group_content_insert(GroupContentInterface $entity) {
  group_bonus_invalidate_cache_for_group_content($entity);
}

/**
 * Implements hook_ENTITY_TYPE_delete().
 */
function group_bonus_group_content_delete(GroupContentInterface $entity) {
  group_bonus_invalidate_cache_for_group_content($entity);
}

/**
 * Invalidates the cache tags for the group of a group content entity.
 *
 * @param \Drupal\group\Entity\GroupContentInterface $entity
 *   The group content entity.
 */
function group_bonus_invalidate_cache_for_group_content(GroupContentInterface $entity) {
  $group = $entity->getGroup();
  Cache::invalidateTags(['group:' . $group->id()]);
}

/**
 * Implements hook_entity_presave().
 */
function group_bonus_entity_presave(EntityInterface $entity) {
  // If the entity has the field_search_title available, then we will populate
  // it with the title of the entity (label) and append the name of the group,
  // if the entity is part of a group.
  if ($entity instanceof FieldableEntityInterface) {
    if ($entity->hasField('field_search_title')) {
      $new_field_value = $entity->label();
      if (!$entity->isNew()) {
        $group_content = GroupContent::loadByEntity($entity);
        if (!empty($group_content)) {
          /** @var \Drupal\group\Entity\GroupContentInterface $group_content */
          $group_content = reset($group_content);
          $new_field_value .= ' ' . $group_content->getGroup()->label();
        }
      }
      $entity->set('field_search_title', $new_field_value);
    }
  }
}
